# Migrer la base de donnée pour créer les tables
- python manage.py migrate

# Environnement
Environment variables : *PYTHONUNBUFFERED=1;DJANGO_SETTINGS_MODULE=Django.settings*

Version de Python : 3.7.0

Pour le reste des dépendences, lancer
`pip install -r requirements.txt` à la racine du projet (mobile-recognition-project/Django)

# Administration
admin:dighimlebg


# Parsing User-Agent
https://stackoverflow.com/questions/2669294/how-to-detect-browser-type-in-django

# If we want to display a file from the server
https://docs.djangoproject.com/fr/3.0/ref/request-response/#fileresponse-objects
_
# Make searchs
https://docs.djangoproject.com/fr/2.2/topics/db/queries/#making-queries
_
# Models
https://django-story.readthedocs.io/en/latest/src/009.models.html

# Datasets
http://www.cs.utoronto.ca/~kriz/cifar.html
