from django.contrib import admin

# Register your models here.
from mobile_recognition.models import Image

admin.site.register(Image)
