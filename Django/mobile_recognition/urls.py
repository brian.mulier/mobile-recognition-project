from django.urls import path

from . import views

urlpatterns = [
    path('', views.MyView.as_view(), name='index'),
    # ex: /api/7/
    path('<int:image_id>/', views.MyViewImage.as_view(), name='preview'),
]
