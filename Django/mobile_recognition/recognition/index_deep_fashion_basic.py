import os

import h5py
import numpy as np

from mobile_recognition.recognition.extract_cnn_vgg16_keras import VGGNet

'''
 Extract features and index the images
'''


def main():
    file_list_eval = 'Eval/list_eval_partition.txt'
    with open(file_list_eval) as f:
        lines = f.readlines()
    lines = lines[2:]
    print("--------------------------------------------------")
    print("         Feature extraction starts")
    print("--------------------------------------------------")
    feats = []
    names = []
    paths = []
    model = VGGNet()
    for i, line in enumerate(lines):
        if i % 100 == 0:
            print("Extracting features from image No. %d , %d images in total" % ((i + 1), len(lines)))
        img_path, item_id, evaluation = line.split()
        if evaluation == "train":
            try:
                norm_feat = model.extract_feat(img_path)
            except FileNotFoundError:
                print("The file {} does not exist".format(img_path))
                continue
            img_name = os.path.split(img_path)[1]
            feats.append(norm_feat)
            names.append(img_name)
            paths.append(img_path[4:])

    feats = np.array(feats)
    output = 'featureCNN_basic.h5'
    print("--------------------------------------------------")
    print("      Writing feature extraction results ...")
    print("--------------------------------------------------")
    h5f = h5py.File(output, 'w')
    h5f.create_dataset('dataset_feat', data=feats)
    paths = [path.encode('utf8') for path in paths]
    h5f.create_dataset('dataset_path', data=paths)
    h5f.close()


if __name__ == '__main__':
    main()
