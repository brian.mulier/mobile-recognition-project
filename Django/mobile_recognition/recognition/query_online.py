import io
import json
import os
from json import JSONEncoder

import h5py
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy
import numpy as np
from PIL import Image

from Django import settings
from mobile_recognition.recognition.extract_cnn_vgg16_keras import VGGNet


class RetrievedImage(object):
    def __init__(self, image_url, score):
        self.image_url = image_url
        self.score = score


class RetrievedImageEncoder(JSONEncoder):
    def default(self, object):
        if isinstance(object, RetrievedImage):
            return object.__dict__
        else:
            # Call base class implementation which takes care of
            # raising exceptions for unsupported types
            return json.JSONEncoder.default(self, object)


def search_images(img, number_of_matching):
    path = 'featureCNN_cropped.h5'
    tmp_file = os.path.join(settings.RECOGNITION_ROOT, path)
    h5f = h5py.File(tmp_file, 'r')
    feats = h5f['dataset_feat'][:]
    paths = h5f['dataset_path'][:]
    h5f.close()

    print("--------------------------------------------------")
    print("               searching starts")
    print("--------------------------------------------------")

    # Display the queried image
    img = Image.open(io.BytesIO(img))
    img = img.resize((settings.IMAGE_SIZE, settings.IMAGE_SIZE))
    queryImg = numpy.asarray(img)
    plt.figure()
    plt.subplot(2, 1, 1)
    plt.imshow(queryImg)
    plt.title("Query Image")
    plt.axis('off')

    # Initialize the VGGNet16 model
    model = VGGNet()

    # Extract query image's feature, compute similarity score and sort
    query_vec = model.extract_feat_from_image(queryImg)
    scores = np.dot(query_vec, feats.T)
    rank_id = np.argsort(scores)[::-1]

    # Number of top retrieved images to show
    image_score_list = [scores[index] for i, index in enumerate(rank_id[0:number_of_matching])]
    image_path = [paths[index] for i, index in enumerate(rank_id[0:number_of_matching])]

    # Append /static/ on each image to create the path on the server to display the images
    final_list = [RetrievedImage("/static/" + str(filename.decode()), score) for filename, score in
                  zip(image_path, image_score_list)]

    # Show the 3 best matching images in matplotlib
    image_path = image_path[:3]
    for i, im in enumerate(image_path):
        image = mpimg.imread(os.path.join(settings.DATASET_ROOT, im.decode()))
        plt.subplot(2, 3, i + 4)
        plt.imshow(image)
        plt.title("search output %d" % (i + 1))
        plt.axis('off')
    plt.show()
    return final_list
