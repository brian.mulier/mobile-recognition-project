import numpy as np
from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input
from keras.preprocessing import image
from numpy import linalg as la

from Django import settings

'''
Create and initialize a VGG16 model
'''


class VGGNet:
    def __init__(self):
        self.input_shape = (settings.IMAGE_SIZE, settings.IMAGE_SIZE, 3)
        self.weight = 'imagenet'
        self.pooling = 'max'
        self.model = VGG16(weights=self.weight,
                           input_shape=(self.input_shape[0], self.input_shape[1], self.input_shape[2]),
                           pooling=self.pooling, include_top=False)
        self.model.predict(np.zeros((1, settings.IMAGE_SIZE, settings.IMAGE_SIZE, 3)))

    '''
    Use vgg16 model to extract features
    Output normalized feature vector
    '''

    def extract_feat(self, img_path):
        img = image.load_img(img_path, target_size=(self.input_shape[0], self.input_shape[1]))
        return self.extract_feat_from_image(img)

    '''
    Use vgg16 model to extract features from an image
    Output normalized feature vector
    '''

    def extract_feat_from_image(self, img):
        img = image.img_to_array(img)
        img = np.expand_dims(img, axis=0)
        img = preprocess_input(img)
        feat = self.model.predict(img)
        norm_feat = feat[0] / la.norm(feat[0])
        return norm_feat
