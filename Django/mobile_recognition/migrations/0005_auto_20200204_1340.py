# Generated by Django 3.0.2 on 2020-02-04 12:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mobile_recognition', '0004_auto_20200204_1104'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='image',
            name='category',
        ),
        migrations.RemoveField(
            model_name='image',
            name='filename',
        ),
    ]
