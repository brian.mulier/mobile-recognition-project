from django.apps import AppConfig


class MobileRecognitionConfig(AppConfig):
    name = 'mobile_recognition'
