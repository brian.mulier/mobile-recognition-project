import json

from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import ensure_csrf_cookie

from .models import Image
from .recognition.query_online import search_images, RetrievedImageEncoder


class MyView(View):
    @method_decorator([csrf_protect, ensure_csrf_cookie])
    def get(self, request):
        return HttpResponse("Welcome on our mobile recognition project")

    @method_decorator([ensure_csrf_cookie])
    def post(self, request):
        print(request)
        try:
            number_of_matching_wanted = 100
            data = request.FILES['uploadedfile']
            client = request.META['HTTP_USER_AGENT']
            if client is None:
                client = 'NO USER-AGENT SENT'
            images_matching = search_images(data.read(), number_of_matching_wanted)
            for im in images_matching:
                im.score = im.score.item()
            json_string = RetrievedImageEncoder().encode(images_matching)
            image = Image(client=client, matches=json_string)
            image.save()
            response = HttpResponse("Your request has been successfully created."
                                    " See the Location header to show the result",
                                    status=201)
            response['Location'] = "/api/{}/".format(image.pk)
            print('Location : /api/{}/'.format(image.pk))
            return response
        except Exception as e:
            print("Exception :", e)
            pass


class MyViewImage(View):
    @method_decorator([ensure_csrf_cookie])
    def get(self, request, image_id):
        image = get_object_or_404(Image, pk=image_id)
        images_matching = json.loads(image.matches)
        data = {
            'id': image_id,
            'client': image.client,
            'date': image.date,
            'results': images_matching
        }
        return JsonResponse(data)
