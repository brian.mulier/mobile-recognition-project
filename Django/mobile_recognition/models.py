from django.db import models


# Create your models here.

class Image(models.Model):
    date = models.DateTimeField(auto_now_add=True, blank=True)
    client = models.CharField(max_length=300, blank=True)
    matches = models.CharField(max_length=20000, null=True)

    def __str__(self):
        return "Date :" + str(
            self.date) + ", client :" + self.client + ", matches :" + self.matches
