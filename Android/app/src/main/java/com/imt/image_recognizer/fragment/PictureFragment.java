package com.imt.image_recognizer.fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.imt.image_recognizer.R;
import com.imt.image_recognizer.services.RestTask;
import com.soundcloud.android.crop.Crop;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.imt.image_recognizer.services.RestTask.CSRFTOKEN_KEY;
import static com.imt.image_recognizer.services.RestTask.REST_URL;

public class PictureFragment extends Fragment {
    static final String TAG = "PictureFragment";

    public static final String ACTION_FOR_CSRF_TOKEN = "GET_CSRF_TOKEN";

    private static final String ACTION_FOR_UPLOAD_PICTURE = "UPLOAD_PICTURE_INTENT";

    private boolean isWaitingForToken = false;

    private Uri currentPictureURI;

    private ProgressDialog progress;
    private ImageView picturePreview;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        getCSRFToken();

        View view = inflater.inflate(R.layout.fragment_picture, container, false);
        picturePreview = view.findViewById(R.id.pictureTaken);
        Button confirmButton = view.findViewById(R.id.btnRoundConfirm);
        Button cropButton = view.findViewById(R.id.btnRoundCrop);

        confirmButton.setOnClickListener(this::uploadPicture);
        cropButton.setOnClickListener(this::startCropping);

        Bundle arguments = getArguments();
        if (arguments != null) {
            currentPictureURI = arguments.getParcelable("pictureURI");
            picturePreview.setImageURI(currentPictureURI);
        }

        return view;
    }

    private void getCSRFToken() {
        RestTask restTask = null;

        try {
            restTask = new RestTask(new WeakReference<>(getActivity()), ACTION_FOR_CSRF_TOKEN, new URL(REST_URL), "GET", null, null, null);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if (restTask != null) {
            restTask.execute();
        }
    }

    private void startCropping(@SuppressWarnings("UnusedParameters") View view) {
        Crop.of(currentPictureURI, currentPictureURI).start(getActivity(), this);
    }

    private byte[] getPictureBytes() {
        Drawable drawable = picturePreview.getDrawable();

        BitmapDrawable bitmapDrawable = ((BitmapDrawable) drawable);
        Bitmap bitmap = bitmapDrawable.getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        return stream.toByteArray();
    }

    @Override
    public void onResume() {
        super.onResume();

        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.registerReceiver(callbackReceiver, new IntentFilter(ACTION_FOR_CSRF_TOKEN));
            activity.registerReceiver(callbackReceiver, new IntentFilter(ACTION_FOR_UPLOAD_PICTURE));
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.unregisterReceiver(callbackReceiver);
        }
    }


    /**
     * Our Broadcast Receiver. We get notified that the data is ready this way.
     */
    private BroadcastReceiver callbackReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Map<String, String> responseProperties = RestTask.lastSearchResponse;

            if (intent.getAction() != null) {
                switch (intent.getAction()) {
                    case ACTION_FOR_UPLOAD_PICTURE:
                        // clear the progress indicator
                        if (progress != null) {
                            progress.dismiss();
                        }

                        if (responseProperties != null) {
                            switchToSimilarPicturesGallery(responseProperties);
                        }
                        break;
                    case ACTION_FOR_CSRF_TOKEN:
                        if (responseProperties != null) {
                            RestTask.CSRF_TOKEN = responseProperties.get(CSRFTOKEN_KEY);
                            if (isWaitingForToken) {
                                startCropping(null);
                            }
                            isWaitingForToken = false;
                        }
                        break;
                }
            }
        }
    };

    private void switchToSimilarPicturesGallery(Map<String, String> responseProperties) {
        SimilarPicturesFragment similarPicturesFragment = new SimilarPicturesFragment();

        Bundle bundle = new Bundle();

        bundle.putString(RestTask.LOCATION, responseProperties.get(RestTask.LOCATION));
        similarPicturesFragment.setArguments(bundle);

        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null) {
            fragmentManager.beginTransaction().replace(android.R.id.content, similarPicturesFragment, SimilarPicturesFragment.TAG).commit();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Crop.REQUEST_CROP && resultCode == RESULT_OK) {
            picturePreview.setImageURI(null);
            picturePreview.setImageURI(currentPictureURI);

            Toast.makeText(getActivity(), R.string.crop_picture_success, Toast.LENGTH_SHORT).show();
        }
    }


    private void uploadPicture(@SuppressWarnings("unused") View view) {
        Map<String, String> requestProperties = RestTask.requestPropsWithCSRFCookies();
        requestProperties.put("Content-Type", "multipart/form-data; boundary=" + "*****");

        String headers = "Content-Disposition: form-data; name=\"uploadedfile\"; filename=\"picture.jpg\"";

        URL url = null;
        try {
            url = new URL(REST_URL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if (url != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                RestTask restTask = new RestTask(new WeakReference<>(activity), ACTION_FOR_UPLOAD_PICTURE, url, "POST", headers, requestProperties, new ByteArrayInputStream(getPictureBytes()));
                restTask.execute();
                progress = ProgressDialog.show(activity, activity.getString(R.string.network_request_title), activity.getString(R.string.uploading_image), true);
            }
        }
    }
}
