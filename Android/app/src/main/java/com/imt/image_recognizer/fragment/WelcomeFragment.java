package com.imt.image_recognizer.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.imt.image_recognizer.R;

public class WelcomeFragment extends Fragment {
    public static final String TAG = "WelcomeFragment";

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_welcome, container, false);
    }

    /**
     * Any code to access activity fields must be handled in this method.
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FragmentActivity activity = getActivity();
        Button nextStep;
        if (activity != null) {
            nextStep = activity.findViewById(R.id.btnRound);
            nextStep.setOnClickListener(this::switchToInstructionsFragment);
        }
    }

    private void switchToInstructionsFragment(@SuppressWarnings("UnusedParameters") View view) {
        InstructionsFragment instructionsFragment = new InstructionsFragment();
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null) {
            fragmentManager.beginTransaction().replace(android.R.id.content, instructionsFragment, InstructionsFragment.TAG).commit();
        }
    }
}
