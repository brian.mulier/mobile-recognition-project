package com.imt.image_recognizer.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.imt.image_recognizer.R;
import com.imt.image_recognizer.model.SimilarPicture;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.imt.image_recognizer.services.RestTask.SERVER_URL;

public class SimilarPicturesAdapter extends BaseAdapter {

    private final Context mContext;
    private final List<SimilarPicture> similarPicturesList;

    public SimilarPicturesAdapter(Context context, List<SimilarPicture> similarPicturesList) {
        this.mContext = context;
        this.similarPicturesList = similarPicturesList;
    }

    @Override
    public int getCount() {
        return similarPicturesList.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public SimilarPicture getItem(int position) {
        return similarPicturesList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final SimilarPicture similarPicture = similarPicturesList.get(position);

        if (convertView == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.similar_picture, parent, false);
        }

        final ImageView imageView = convertView.findViewById(R.id.pictureThumbnail);
        final ProgressBar scoreBar = convertView.findViewById(R.id.scoreBar);
        final TextView scorePercent = convertView.findViewById(R.id.similarityPercent);

        //Picture loading with placeholder and error picture if failing
        Picasso.get()
                .load(SERVER_URL + similarPicture.getImage_url())
                .centerCrop()
                .placeholder(R.drawable.loading)
                .error(R.drawable.error_loading)
                .fit()
                .tag(mContext)
                .into(imageView);

        //Set progressbar and percent text
        int similarity = (int) (similarPicture.getScore() * 100);
        scoreBar.setProgress(similarity);
        Resources res = mContext.getResources();
        scorePercent.setText(String.format(res.getString(R.string.percentage), similarity));

        return convertView;
    }

}