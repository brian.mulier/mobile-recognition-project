package com.imt.image_recognizer.model;

@SuppressWarnings("unused")
public class SimilarPicture {
    private String image_url;
    private double score;

    public SimilarPicture(String image_url, double score) {
        this.image_url = image_url;
        this.score = score;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}
