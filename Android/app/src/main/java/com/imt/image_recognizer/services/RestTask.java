package com.imt.image_recognizer.services;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.util.Log;

import com.imt.image_recognizer.fragment.PictureFragment;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.CookieManager;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class RestTask extends AsyncTask<HttpURLConnection, Void, Map<String, String>> {
    private static final String TAG = "RestTask";
    private static final String lineEnd = "\r\n";

    public static String SERVER_URL;
    public static String REST_URL;

    public static final String HTTP_RESPONSE = "httpResponse";
    public static final String LOCATION = "Location";
    public static final String CSRFTOKEN_KEY = "csrftoken";

    public static Map<String, String> lastSearchResponse = null;

    public static String CSRF_TOKEN;


    private WeakReference<Context> mContext;
    private String mAction;
    private URL mURL;
    private String mMethod;
    private String mHeaders;
    private Map<String, String> mRequestProperties;
    private ByteArrayInputStream mData;

    public RestTask(WeakReference<Context> context, String action, URL url, String method, String headers, Map<String, String> requestProperties, ByteArrayInputStream data) {
        mContext = context;
        mAction = action;
        mURL = url;
        mMethod = method;
        mHeaders = headers;
        mRequestProperties = requestProperties;
        mData = data;
    }

    public static void retrieveServerIPFromProperties(Context context){
        Properties properties = new Properties();
        AssetManager assetManager = context.getAssets();
        InputStream inputStream;
        try {
            inputStream = assetManager.open("app.properties");
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        SERVER_URL = properties.getProperty("ip_server");
        REST_URL = SERVER_URL + properties.getProperty("rest_endpoint");
    }

    public static Map<String, String> requestPropsWithCSRFCookies() {
        Map<String, String> requestProperties = new HashMap<>();
        requestProperties.put("Connection", "Keep-Alive");
        requestProperties.put("X-CSRFToken", CSRF_TOKEN);
        requestProperties.put("Cookie", CSRFTOKEN_KEY + "=" + CSRF_TOKEN);
        return requestProperties;
    }

    @Override
    protected Map<String, String> doInBackground(HttpURLConnection... params) {
        Map<String, String> responseProperties = new HashMap<>();

        //------------------ read the SERVER RESPONSE
        try {
            // Open a HTTP connection to the URL
            HttpURLConnection conn = (HttpURLConnection) mURL.openConnection();

            if (conn != null) {
                switch (mMethod) {
                    case "GET":
                        // Allow Inputs
                        conn.setDoInput(true);
                        conn.setDoOutput(false);
                        break;
                    case "POST":
                        conn.setDoInput(true);
                        conn.setDoOutput(true);
                        break;
                }
                // Don't use a cached copy.
                conn.setUseCaches(false);
                // Use a post method.
                conn.setRequestMethod(mMethod);
                if (mRequestProperties != null) {
                    for (Map.Entry<String, String> entry : mRequestProperties.entrySet()) {
                        conn.setRequestProperty(entry.getKey(), entry.getValue());
                    }
                }

                if (mHeaders != null && mData != null) {
                    DataOutputStream dos;
                    dos = new DataOutputStream(conn.getOutputStream());

                    String boundary = "*****";
                    String twoHyphens = "--";

                    if (mHeaders != null) {
                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes(mHeaders + lineEnd);
                        dos.writeBytes(lineEnd);
                    }

                    if (mData != null) {
                        writeInputStreamInRequest(dos);
                        dos.writeBytes(lineEnd);
                        dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                    }
                }

                if (PictureFragment.ACTION_FOR_CSRF_TOKEN.equals(mAction)) {
                    final String COOKIES_HEADER = "Set-Cookie";
                    CookieManager msCookieManager = new java.net.CookieManager();
                    Map<String, List<String>> headerFields = conn.getHeaderFields();
                    List<String> cookiesHeader = headerFields.get(COOKIES_HEADER);

                    if (cookiesHeader != null) {
                        for (String cookie : cookiesHeader) {
                            msCookieManager.getCookieStore().add(null, HttpCookie.parse(cookie).get(0));
                        }
                    }

                    CookieStore cookieStore = msCookieManager.getCookieStore();
                    List<HttpCookie> cookies = cookieStore.getCookies();

                    for (HttpCookie cookie : cookies) {
                        if (cookie.getName().equals(CSRFTOKEN_KEY)) {
                            responseProperties.put(CSRFTOKEN_KEY, cookie.getValue());
                        }
                    }
                }

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String str;
                StringBuilder result = new StringBuilder();
                while ((str = bufferedReader.readLine()) != null) {
                    result.append(str);
                    Log.e("Debug", "Server Response " + str);
                }
                bufferedReader.close();

                responseProperties.put(HTTP_RESPONSE, result.toString());
                responseProperties.put(LOCATION, conn.getHeaderField(LOCATION));
                return responseProperties;
            }
        } catch (
                IOException ioex) {
            Log.e("Debug", "error: " + ioex.getMessage(), ioex);
        }

        responseProperties.put("error", "Error while processing request");
        return responseProperties;
    }

    private void writeInputStreamInRequest(DataOutputStream dos) {
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1024 * 1024;
        bytesAvailable = mData.available();
        bufferSize = Math.min(bytesAvailable, maxBufferSize);
        buffer = new byte[bufferSize];

        try {
            // read file and write it into form...
            bytesRead = mData.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                dos.write(buffer, 0, bufferSize);

                bytesAvailable = mData.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = mData.read(buffer, 0, bufferSize);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * `onPostExecute` is run after `doInBackground`, and it's
     * run on the main/ui thread, so you it's safe to update ui
     * components from it. (this is the correct way to update ui
     * components.)
     */
    @Override
    protected void onPostExecute(Map<String, String> result) {
        Log.i(TAG, "RESULT = " + result);
        Intent intent = new Intent(mAction);

        lastSearchResponse = result;

        // broadcast the completion
        mContext.get().sendBroadcast(intent);
    }
}