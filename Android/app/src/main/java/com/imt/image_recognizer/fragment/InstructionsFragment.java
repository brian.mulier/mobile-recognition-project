package com.imt.image_recognizer.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatImageButton;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.imt.image_recognizer.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class InstructionsFragment extends Fragment {
    public static final String TAG = "InstructionsFragment";

    private String mCurrentPhotoPath = "";

    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_IMAGE_CHOOSE = 2;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_instructions, container, false);

        AppCompatImageButton takePictureButton = view.findViewById(R.id.takePictureButton);
        AppCompatImageButton choosePictureButton = view.findViewById(R.id.choosePictureButton);

        takePictureButton.setOnClickListener(this::switchToCamera);
        choosePictureButton.setOnClickListener(this::switchToGallery);

        return view;
    }

    private void switchToCamera(@SuppressWarnings("UnusedParameters") View view) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    // Error occurred while creating the File
                    Log.i(TAG, "IOException");
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(activity,
                            "com.imt.image_recognizer.fileprovider",
                            photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        }
    }

    private void switchToGallery(@SuppressWarnings("UnusedParameters") View view) {
        Intent choosePictureIntent = new Intent();
        choosePictureIntent.setType("image/*");

        FragmentActivity activity = getActivity();
        if (activity != null) {

            if (choosePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
                if (Build.VERSION.SDK_INT < 19) {
                    choosePictureIntent.setAction(Intent.ACTION_GET_CONTENT);
                } else {
                    choosePictureIntent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                    choosePictureIntent.addCategory(Intent.CATEGORY_OPENABLE);
                }
                startActivityForResult(Intent.createChooser(choosePictureIntent, "Select Picture"), REQUEST_IMAGE_CHOOSE);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Bitmap imageBitmap = null;
            FragmentActivity activity = getActivity();
            Uri uri = null;
            if (activity != null) {
                switch (requestCode) {
                    case REQUEST_IMAGE_CAPTURE:
                        uri = Uri.parse(mCurrentPhotoPath);
                        break;
                    case REQUEST_IMAGE_CHOOSE:
                        uri = data.getData();
                }
            }

            if (uri != null) {
                PictureFragment pictureFragment = new PictureFragment();
                Bundle bitmapBundle = new Bundle();
                bitmapBundle.putParcelable("pictureURI", uri);
                pictureFragment.setArguments(bitmapBundle);

                FragmentManager fragmentManager = getFragmentManager();
                if (fragmentManager != null) {
                    fragmentManager.beginTransaction().replace(android.R.id.content, pictureFragment, PictureFragment.TAG).commit();
                }
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.FRANCE).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        FragmentActivity activity = getActivity();
        if(activity != null) {
            File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File image = File.createTempFile(
                    imageFileName,  // prefix
                    ".jpg",         // suffix
                    storageDir      // directory
            );

            // Save a file: path for use with ACTION_VIEW intents
            mCurrentPhotoPath = "file:" + image.getAbsolutePath();
            return image;
        }

        return null;
    }
}
