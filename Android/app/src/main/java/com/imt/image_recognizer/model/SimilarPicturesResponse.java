package com.imt.image_recognizer.model;

import java.util.List;

@SuppressWarnings("unused")
public class SimilarPicturesResponse {
    private int id;
    private String client;
    private String date;
    private List<SimilarPicture> results;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<SimilarPicture> getResults() {
        return results;
    }

    public void setResults(List<SimilarPicture> results) {
        this.results = results;
    }
}
