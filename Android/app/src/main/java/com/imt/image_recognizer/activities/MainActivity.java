package com.imt.image_recognizer.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.imt.image_recognizer.fragment.InstructionsFragment;
import com.imt.image_recognizer.fragment.PictureFragment;
import com.imt.image_recognizer.fragment.WelcomeFragment;
import com.imt.image_recognizer.services.RestTask;
import com.soundcloud.android.crop.Crop;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        //Init rest client with properties
        RestTask.retrieveServerIPFromProperties(this);

        if (savedInstanceState == null) {
            WelcomeFragment welcomeFragment = new WelcomeFragment();
            getSupportFragmentManager().beginTransaction().add(android.R.id.content, welcomeFragment, WelcomeFragment.TAG).commit();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        InstructionsFragment fragment = (InstructionsFragment) getSupportFragmentManager().findFragmentByTag(InstructionsFragment.TAG);
        //Handle back button to return to instructions page
        if (keyCode == KeyEvent.KEYCODE_BACK && (fragment == null || !fragment.isVisible())) {
            fragment = new InstructionsFragment();
            getSupportFragmentManager().beginTransaction().replace(android.R.id.content, fragment, InstructionsFragment.TAG).commit();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
