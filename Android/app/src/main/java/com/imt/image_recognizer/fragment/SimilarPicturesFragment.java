package com.imt.image_recognizer.fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.google.gson.Gson;
import com.imt.image_recognizer.R;
import com.imt.image_recognizer.adapter.SimilarPicturesAdapter;
import com.imt.image_recognizer.model.SimilarPicturesResponse;
import com.imt.image_recognizer.services.RestTask;

import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import static com.imt.image_recognizer.services.RestTask.HTTP_RESPONSE;
import static com.imt.image_recognizer.services.RestTask.SERVER_URL;
import static com.imt.image_recognizer.services.RestTask.requestPropsWithCSRFCookies;

public class SimilarPicturesFragment extends Fragment {
    static final String TAG = "SimilarPicturesFragment";

    private static final String ACTION_FOR_SIMILAR_PICTURES_CALLBACK = "SIMILAR_PICTURES_REQUEST";

    private ProgressDialog progress;

    private GridView similarPicturesGrid;

    @Override
    public void onResume() {
        super.onResume();

        FragmentActivity activity = getActivity();
        if(activity != null) {
            activity.registerReceiver(callbackReceiver, new IntentFilter(ACTION_FOR_SIMILAR_PICTURES_CALLBACK));
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        FragmentActivity activity = getActivity();
        if(activity != null){
            activity.unregisterReceiver(callbackReceiver);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_similar_pictures, container, false);
        similarPicturesGrid = view.findViewById(R.id.similarPictures);

        Bundle bundle = getArguments();
        String location = null;
        if (bundle != null) {
            location = bundle.getString(RestTask.LOCATION);
            bundle.clear();
        }

        RestTask restTask;
        try {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                restTask = new RestTask(new WeakReference<>(activity), ACTION_FOR_SIMILAR_PICTURES_CALLBACK, new URL(SERVER_URL + location), "GET", null, requestPropsWithCSRFCookies(), null);
                restTask.execute();
                progress = ProgressDialog.show(activity, activity.getString(R.string.network_request_title), activity.getString(R.string.retrieving_similar_pictures_text), true);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return view;
    }

    /**
     * Our Broadcast Receiver. We get notified that the data is ready this way.
     */
    private BroadcastReceiver callbackReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // clear the progress indicator
            if (progress != null) {
                progress.dismiss();
            }

            if (ACTION_FOR_SIMILAR_PICTURES_CALLBACK.equals(intent.getAction())) {
                Gson deserializer = new Gson();

                Map<String, String> responseProperties = RestTask.lastSearchResponse;
                SimilarPicturesResponse similarPicturesResponse;
                if (responseProperties != null) {
                    similarPicturesResponse = deserializer.fromJson(responseProperties.get(HTTP_RESPONSE), SimilarPicturesResponse.class);
                    SimilarPicturesAdapter similarPicturesAdapter = new SimilarPicturesAdapter(getActivity(), similarPicturesResponse.getResults());
                    similarPicturesGrid.setAdapter(similarPicturesAdapter);
                }
            }
        }
    };
}
